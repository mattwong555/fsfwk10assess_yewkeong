//Load the libraries
var path = require("path");
var express = require("express");

//Create an instance of the application
var app = express();

//master order
var list = [];

//Define routes
//
var register = function(ord) {
	return ({
		email: ord.email,
		password: ord.password,
		name: ord.name,
		gender: ord.gender,
		dob: ord.dob,
		address: ord.address,
        country: ord.country,
        contact: ord.contact,
	});
}

//The parameters are going to be in a query string
app.get("/register", function(req, resp) {

	list.push(register(req.query));

	console.log("All orders:\n %s", JSON.stringify(list));

	resp.status(201).end();

});



app.use("/libs", express.static(path.join(__dirname, "bower_components")));

app.use(express.static(path.join(__dirname, "public")));


//Setup the server
app.set("port", process.env.APP_PORT || 3000);

app.listen(app.get("port"), function() {
	console.log("Application started at %s on port %d"
			, new Date(), app.get("port"));
});
