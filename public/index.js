//Place order
(function () {
		var RegisterApp = angular.module("RegisterApp", []);

		var initForm = function (ctrl) {
			ctrl.email = "";
			ctrl.password = "";
			ctrl.name = "";
			ctrl.gender = "";
			ctrl.dob = "";
			ctrl.address = "";
			ctrl.country = "";
			ctrl.contact = 0;
		}

		var createQueryString = function (ctrl) {
			return ({
				email: ctrl.email,
				password: ctrl.password,
				name: ctrl.name,
				gender: ctrl.gender,
				dob: ctrl.dob,
				address: ctrl.address,
				country: ctrl.country,
				contact: ctrl.contact
			});
		}

		var RegisterCtrl = function ($http) {
			var registerCtrl = this;

			initForm(registerCtrl);

	//		registerCtrl.isValidDob = function() {
		//		var status = false;
			//if(getFullYear(registerCtrl.dob)< 2000) { facing problem as the date format defined as string isnt "tight enough" for getFullYear to work
			
		//		status=true;
	//		}
	//			return status;
	//		}
		
		
		
			registerCtrl.isValidContact = function() {
				var status = false;
			if(isNaN(registerCtrl.contact)) {
				status=true;
			}
				return status;
			}

			registerCtrl.submitBtn = function () {
				$http.get("/register", {
						params: createQueryString(registerCtrl)
					 
                })
                .then(function (result) {
                   // console.log(JSON.stringify(result.data));
                    registerCtrl.status = "Registeration successful";

                })
                .catch(function (status) {
                    registerCtrl.status = "Username and Password incorrect";
                });
				initForm(registerCtrl);
				}
			}


			RegisterApp.controller("RegisterCtrl", ["$http", RegisterCtrl]);
		})();